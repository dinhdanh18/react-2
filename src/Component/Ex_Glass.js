import React, { Component } from 'react'
import { glassArr } from './data'
import shortid from "shortid"

export default class Ex_Glass extends Component {
  state = {
      glassArr : glassArr,
        glassDetail : glassArr[0]
    }
    renderGlass = () => {
      return this.state.glassArr.map((item,index) => {
        let randomNumBer = shortid.generate();
        return <div className='col-4' key={randomNumBer}><img onClick={() => {this.handleGlass(index)}} src={item.url} alt="" /></div>
      })
    }
    handleGlass = (index) => { 
      this.setState({glassDetail : glassArr[index]})
     }
  render() {
    return (
      <div>
        <div className="content">
        <div className="header"><span>try glasses app online</span></div>
        <div className='content-modal my-5'>
          <div className='content-modal-img'>
            <img src="./glassesImage/model.jpg"  className='img'alt="" />
            <img src={this.state.glassDetail.url} className='img-glass' alt="" />
            <div className='detail'>
              <h1>{this.state.glassDetail.name}</h1>
              <p>{this.state.glassDetail.desc}</p>
            </div>
          </div>
          <div className='content-modal-img'>
            <img src="./glassesImage/model.jpg"  className='img'alt="" />
          </div>
        </div>
        <div className='list-glass'>
          <div className='row'>
          {this.renderGlass()}
          </div>
        </div>
        </div>
      </div>
    )
  }
}
